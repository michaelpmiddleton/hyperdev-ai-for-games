# __EGP 410: AI For Games__  
This repository comprises the files for the "AI For Games" (EGP-410) course I took while studying abroad in Montreal in Fall 2017. It is forked from the [default class repository](https://github.com/zap-twiz/EGP-410).    
<br>
<div align="center">
![Screenshot of a assignment 4.](https://github.com/michaelpmiddleton/ai-for-games-coursework/blob/master/screenshot.png")
</div>
<br>

## _Contents:_  
The default (pre-forked) configuration of this repository contains 3 major directories:  
 * DeanLib - Base utility library provided by Dean Lawson. Memory tracking and basic vector mathematics utilities.
 * External Libs - Allegro 5.0.10 support libraries.
 * Game AI - Base source code for EGP 410 Assignments
   * pathfinding
   * state machine
   * steering  

<br>  

## _Want to Fork?_    
Please feel free to use my code as a reference for your own work. With that said, at this time I am requesting that you do not _copy_ this code. Thank you for your understanding and happy hacking!  

You might also want to consider forking from the original repository like I did. If you'd like to do so, go to the link in the description part of this README file.
<br><br>

## _Got Questions?_ 
I'm happy to answer them! Just [send me an email](mailto:mp.middleton@outlook.com)!
